from django.db import models
from django.contrib.auth.models import User
# Create your models here.


class Item(models.Model):
    """
    Item Model:
    """

    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100, blank = False)
    comment = models.CharField(max_length=300,unique=False,blank=False)
    price = models.IntegerField();


    def __unicode__(self):
        return self.name