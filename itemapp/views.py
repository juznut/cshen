from django.contrib.auth.models import User
from django.forms import ModelForm
from django import forms
from django.forms.fields import CharField
class UserCreateForm(ModelForm):
       password = CharField(widget=forms.PasswordInput())
       class Meta:
               model = User;
               exclude = ['is_staff','is_active','is_superuser','last_login','date_joined','user_permissions','groups'];
# Create your views here.
from datetime import *
from itemapp.models import Item
from django.contrib.auth.models import User
from django.views.generic import CreateView, ListView, UpdateView, DeleteView, DetailView
from django.http import HttpResponseRedirect
from itemapp.forms import UserCreateForm

class UserCreateView(CreateView):
   model = User
   template_name = 'registration/create.html'
   success_url = '/'
   form_class = UserCreateForm

   def form_valid(self,form): #override the current form_valid method to insert some data before saving the new user
       self.object = form.save(commit=False); #stop the save
       self.object.set_password(self.object.password); #hash the users password
       self.object.is_staff = 0; #set the user as a regular user
       self.object.is_active = 1; #set the user as active
       self.object.is_superuser = 0; #set the user as non-super user
       self.object.last_login = datetime.today(); #set the users last login time as "now"
       self.object.date_joined = datetime.today(); #set the users registration date as "now"
       self.object.save(); #save the user
       return HttpResponseRedirect(self.success_url) #redirect to success_url

class ItemListView(ListView):
   model = Item
   template_name = 'index.html'

class ItemDetailView(DetailView):
   model = Item
   template_name = 'detail.html'
class ItemCreateView(CreateView):
   model = Item
   template_name = 'create.html'
   success_url = '/'

class ItemUpdateView(UpdateView):
   model = Item
   template_name = 'update.html'
   success_url = '/'

class ItemDeleteView(DeleteView):
   model = Item
   template_name = 'delete.html'
   success_url = '/'
 