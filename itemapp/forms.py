from django.contrib.auth.models import User
from django.forms import ModelForm
from django import forms
from django.forms.fields import CharField
class UserCreateForm(ModelForm):
       password = CharField(widget=forms.PasswordInput())
       class Meta:
               model = User;
               exclude = ['is_staff','is_active','is_superuser','last_login','date_joined','user_permissions','groups'];
 
