from django.conf.urls import patterns, include, url
from itemapp.views import ItemListView, ItemCreateView, ItemUpdateView, ItemDeleteView, ItemDetailView, UserCreateView
from django.contrib.auth.views import login, logout
# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'covetousShen.views.home', name='home'),
    # url(r'^covetousShen/', include('covetousShen.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
	url(r'register', UserCreateView.as_view()),
	url(r'logout',logout),
	url(r'login',login),
	url(r'delete/(?P<pk>\d+)',ItemDeleteView.as_view()),
	url(r'detail/(?P<pk>\d+)',ItemDetailView.as_view()),
	url(r'update/(?P<pk>\d+)',ItemUpdateView.as_view()),
	url(r'create',ItemCreateView.as_view()),
	url(r'',ItemListView.as_view()),
	
)
